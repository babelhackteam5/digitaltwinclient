﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using Amazon;
using Amazon.Lambda.Model;
using Amazon.Lambda;
using Amazon.Auth;
using LitJson;

public class IndustryController : MonoBehaviour {

    public GameObject sensor1;
    public GameObject sensor2;
    public GameObject sensor3;
    public GameObject sensor4;
    public GameObject sensor5;
    public GameObject sensor6;
    public GameObject sensor7;

    public GameObject pos0;
    public GameObject pos1;
    public GameObject pos2;
    public GameObject pos3;
    public GameObject pos4;
    public GameObject pos5;
    public GameObject pos6;

    public float speed1;
    public float speed2;
    public float speed3;
    public float speed4;
    public float speed5;
    public float speed6;

    public float speed7;
    public float speed8;
    public float speed9;


    private Dictionary<string, GameObject> items;
    public GameObject templateItem;

    public long T1;
    public long T2;
    public long T3;
    public long T4;
    public long T5;
    public long T6;

    public long T7;
    public long T8;
    public long T9;

    public float waitTime = 5000;
    private float timer = 0f;

    Vector3 V1;
    Vector3 V2;
    Vector3 V3;
    Vector3 V4;
    Vector3 V5;
    Vector3 V6;

    string timeini = "20171020211949399";
    string timefin = "20171020212256086";
    DateTime timeInit = new DateTime(2017, 10, 20, 21, 19, 49);

    long[] times = { 0, 4790, 9070, 10860, 15130, 20590, 25000, 25000, 25000};
    float[] distancias = { 0f, 9.5218f, 17.8719f, 26.8519f, 32.9919f, 48.7119f, 61.0829f };




    AmazonLambdaClient lambdaClient;

    // Use this for initialization
    void Start () {

        initAWS();

        timeini = timeInit.ToString("yyyyMMddHHmmss") + "000";

        items = new Dictionary<string, GameObject>();

        V1 = pos1.transform.position - pos0.transform.position;
        V2 = pos2.transform.position - pos1.transform.position;
        V3 = pos3.transform.position - pos2.transform.position;
        V4 = pos4.transform.position - pos3.transform.position;
        V5 = pos5.transform.position - pos4.transform.position;
        V6 = pos6.transform.position - pos5.transform.position;



        Debug.Log("V1 " + V1.magnitude);
        Debug.Log("V2 " + V2.magnitude);
        Debug.Log("V3 " + V3.magnitude);
        Debug.Log("V4 " + V4.magnitude);
        Debug.Log("V5 " + V5.magnitude);
        Debug.Log("V6 " + V6.magnitude);
        Debug.Log("V1V2 " + (V2.magnitude + V1.magnitude));
        Debug.Log("V1V2V3 " + (V3.magnitude + V2.magnitude + V1.magnitude));
        Debug.Log("V1V2V3V4 " + (V4.magnitude + V3.magnitude + V2.magnitude + V1.magnitude));
        Debug.Log("V1V2V3V4V5 " + (V5.magnitude + V4.magnitude + V3.magnitude + V2.magnitude + V1.magnitude));
        Debug.Log("V1V2V3V4V5V6 " + (V6.magnitude + V5.magnitude + V4.magnitude + V3.magnitude + V2.magnitude + V1.magnitude));

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey("c") )
        {
            GameObject go = createItem();
            go.SetActive(true);
            
            go.transform.position = pos0.transform.position;


            //go.GetComponent<ItemController>().activeNextStep();
        }

        if (Input.GetKey("i"))
        {
            invokeAWS();
        }

        // Tareas programadas
        timer += Time.deltaTime;
        if (timer > waitTime) {

            timefin = timeInit.AddSeconds(10).ToString("yyyyMMddHHmmss") + "000";

            invokeAWS();
            timer = 0f;
            timeInit = timeInit.AddSeconds(10);
            timeini = timeInit.ToString("yyyyMMddHHmmss") + "000";

        }
    }

    private GameObject createItem() {

        return Instantiate(templateItem);

    }

    

    public float distance(long t)
    {
        if (t > 0 && t <= T1)
        {
            return t * speed1;
        }
        else if (t > T1 && t <= T2)
        {
            return (t-T1) * speed2 + T1 * speed1;
        }
        else if (t > T2 && t <= T3)
        {
            return (t-T2)*speed3 + (T2-T1)*speed2 + T1 * speed1;
        }
        else if (t > T3 && t <= T4)
        {
            return (t - T3) * speed4 + (T3 - T2) * speed3 + (T2 - T1) * speed2 + T1 * speed1;
        }
        else if (t > T4 && t <= T5)
        {
            return (t - T4) * speed5 + (T4 - T3) * speed4 + (T3 - T2) * speed3 + (T2 - T1) * speed2 + T1 * speed1;
        }
        else if (t > T5 && t <= T6)
        {
            return (t - T5) * speed6 + (T5 - T4) * speed5 + (T4 - T3) * speed4 + (T3 - T2) * speed3 + (T2 - T1) * speed2 + T1 * speed1;
        }
        else if (t > T6 && t <= T7)
        {
            return (t - T6) * speed7 + (T6 - T5) * speed6 + (T5 - T4) * speed5 + (T4 - T3) * speed4 + (T3 - T2) * speed3 + (T2 - T1) * speed2 + T1 * speed1;
        }
        else if (t > T7 && t <= T8)
        {
            return (t - T7) * speed8 + (T7 - T8) * speed7 + (T6 - T5) * speed6 + (T5 - T4) * speed5 + (T4 - T3) * speed4 + (T3 - T2) * speed3 + (T2 - T1) * speed2 + T1 * speed1;
        }
        else if (t > T8 && t <= T9)
        {
            return (t - T8) * speed9 + (T8 - T7) * speed8 + (T7 - T8) * speed7 + (T6 - T5) * speed6 + (T5 - T4) * speed5 + (T4 - T3) * speed4 + (T3 - T2) * speed3 + (T2 - T1) * speed2 + T1 * speed1;
        }
        return .0f;
    }

    public Vector3 position(float d)
    {
        if(d > 0 && d <= V1.magnitude)
        {
            return (Vector3.Normalize(V1) * d) + pos0.transform.position;
        }
        else if(d > V1.magnitude && d <= (V1.magnitude + V2.magnitude))
        {
            return (Vector3.Normalize(V2) * (d - V1.magnitude)  + pos0.transform.position + V1);
        }
        else if(d > (V1.magnitude + V2.magnitude) && d <= (V1.magnitude + V2.magnitude + V3.magnitude))
        {
            return (Vector3.Normalize(V3) * (d - V2.magnitude - V1.magnitude))  + pos0.transform.position + V1 + V2;
        }
        else if (d > (V1.magnitude + V2.magnitude + V3.magnitude) && d <= (V1.magnitude + V2.magnitude + V3.magnitude + V4.magnitude))
        {
            return (Vector3.Normalize(V4) * (d - V3.magnitude - V2.magnitude - V1.magnitude)) + pos0.transform.position + V1 + V2 + V3;
        }
        else if (d > (V1.magnitude + V2.magnitude + V3.magnitude + V4.magnitude) && d <= (V1.magnitude + V2.magnitude + V3.magnitude + V4.magnitude + V5.magnitude))
        {
            return (Vector3.Normalize(V5) * (d - V4.magnitude - V3.magnitude - V2.magnitude - V1.magnitude)) + pos0.transform.position + V1 + V2 + V3 + V4;
        }
        else if (d > (V1.magnitude + V2.magnitude + V3.magnitude + V4.magnitude + V5.magnitude) && d <= (V1.magnitude + V2.magnitude + V3.magnitude + V4.magnitude + V5.magnitude + V6.magnitude))
        {
            return (Vector3.Normalize(V6) * (d - V5.magnitude - V4.magnitude - V3.magnitude - V2.magnitude - V1.magnitude)) + pos0.transform.position + V1 + V2 + V3 + V4 + V5;
        }
        return pos0.transform.position;
    }


    // Use this for initialization
    private void initAWS()
    {
        UnityInitializer.AttachToGameObject(this.gameObject);
        Amazon.AWSConfigs.HttpClient = Amazon.AWSConfigs.HttpClientOption.UnityWebRequest;


        //var credentials = new CognitoAWSCredentials("UnityClients", RegionEndpoint.EUWest1);
        lambdaClient = new AmazonLambdaClient("AKIAJ3GE4WHULNP44GJA", "oZYN09awJd31tJpGgRVOIhdHymh6autI29xhA/5h", RegionEndpoint.USEast2);
    }

    private void invokeAWS() {

        var request = new InvokeRequest()
        {
            FunctionName = "IndustrialQueryLambda",
            Payload = "{\"timestamFrom\" : \"" + timeini + "\", \"timestampTo\" : \"" + timefin + "\"}",
            InvocationType = InvocationType.RequestResponse
        };

        print(timeini + ", " + timefin);

        lambdaClient.InvokeAsync(request,
            (responseObject) => {
                if (responseObject.Exception != null)
                    {
                    print(responseObject.Exception);
                }
                else
                {
                    print(responseObject.Response.StatusCode);
                    print(Encoding.ASCII.GetString(responseObject.Response.Payload.ToArray()));
                    JsonData jsdata = Processjson(Encoding.ASCII.GetString(responseObject.Response.Payload.ToArray()));

                    int count = (int)jsdata["items"].Count;
                   
                    for (int i = 0; i < count; i++) {
                        int numsensores = jsdata["items"][i]["sensors"].Count;
                        print("Número de sensores" + numsensores);
                        string id = (string)jsdata["items"][i]["idItem"];
                        int lastsensor = (int)jsdata["items"][i]["sensors"][numsensores-1]["sensororder"];

                   
                        GameObject go = null;
                        if (items.ContainsKey(id))
                        {
                            go = (GameObject)items[id];
                        }
                        
                      
                        if(go != null)
                        {
                            print("encontrado item " + id + " y " + lastsensor);
                            print(id + "->" + times[lastsensor]);
                            go.GetComponent<ItemController>().setOfsset(times[lastsensor] + DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
                        }
                        else
                        {
                            go = (GameObject)Instantiate(templateItem);
                            print("creado item" + id);
                            go.SetActive(true);
                            go.transform.position = pos0.transform.position;                            
                            go.GetComponent<ItemController>().setOfsset(times[lastsensor] );

                            items.Add(id, go);

                        }
                    }

                }
            });
    }

    private JsonData Processjson(string jsonString)
    {
        JsonData data = JsonMapper.ToObject(jsonString);
        //string id = (string)data["items"][0]["sensors"][0]["id"];
       // print("Sergio eres el " + id);
        return data;
    }

}