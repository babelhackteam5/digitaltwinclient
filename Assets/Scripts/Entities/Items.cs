﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Items {

    public IList<Item> items;

    public static Items CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<Items>(jsonString);
    }
}
