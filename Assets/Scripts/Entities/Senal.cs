﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Senal
{

    public string id;
    public string time;

    public static Senal CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<Senal>(jsonString);
    }
}
