﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Item {

    public string id;
    //public IList<Sensor> sensors;

    public static Item CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<Item>(jsonString);
    }

}
